var baseURL = "../../";
var vm = new Vue({
    el: '#cwapp',
    data: {
        q: {
            username: null,
            page: "1",
            limit: "10"
        },
        company:{

        },
        companyId:null,
        companyList:[],
        showCompanyList:false,
        showList: true,
        title: null,
        roleList: {},
        user: {
            status: 1,
            roleIdList: []
        },
        page: {},
        multipleSelection: [],
        rules: {
            username: [
                { required: true, message: '请输入用户名', trigger: 'blur' }
            ],
            password: [
                { required: true, message: '请输入密码', trigger: 'blur' },
                { min: 6, message: '密码最少六位', trigger: 'blur' }
            ],
            roleId: [
                {required: true, message: '请选择角色', trigger: 'change' }
            ]
        }
    },
    methods: {
        selectCompany(user) {
            vm.company.userId=user.userId;
            vm.companyId=user.companyId;
            vm.showCompanyList = true;
        },
        handleSizeChange(val) {
            console.log(val);
            vm.q.limit = val.toString();
            this.loadPage(false);
        },
        handleCurrentChange(val) {
            console.log(val);
            vm.q.page = val.toString();
            this.loadPage(false);
        },
        handleSelectionChange(val) {
            this.multipleSelection = val;
            console.log(this.multipleSelection)
        },
        query: function () {
            this.loadPage(false)
        },
        add: function () {
            vm.showList = false;
            vm.title = "新增";
            vm.roleList = {};
            vm.user = {status: 1, roleIdList: []};
            //获取角色信息
            this.getRoleList();
        },
        update: function (event) {
            var ids=this.getSelectedIds();
            let userId;
            if (ids.length===1){
                userId=ids[0];
            }else {
                this.$message.error('请选择一条记录');
                return;
            }
            vm.showList = false;
            vm.title = "修改";
            vm.getInfo(userId)
            //获取角色信息
            this.getRoleList();
        },
        saveOrUpdate: function (event) {

            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    var url = vm.user.userId == null ? "sys/user/save" : "sys/user/update";
                    $.ajax({
                        type: "POST",
                        url: baseURL + url,
                        contentType: "application/json",
                        data: JSON.stringify(vm.user),
                        // data: vm.user,
                        success: function (r) {
                            if (r.code === 0) {
                                Vue.prototype.$message({
                                    type: 'success',
                                    message: '操作成功!'
                                });
                                vm.loadPage(false);
                            } else {
                                Vue.prototype.$message({
                                    type: 'error',
                                    message: r.msg
                                });
                            }
                        }
                    });
                } else {
                    return false;
                }
            });

        },
        del: function (event) {
            var ids=this.getSelectedIds();
            let userIds;
            if (ids.length===0){
                this.$message.error('请选择一条记录');
                return;
            }else {
                userIds=ids;
            }

            this.$confirm('确定要删除选中的记录?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                $.ajax({
                    type: "POST",
                    url: baseURL + "sys/user/delete",
                    contentType: "application/json",
                    data: JSON.stringify(userIds),
                    success: function (r) {
                        if (r.code === 0) {
                            Vue.prototype.$message({
                                type: 'success',
                                message: '删除成功!'
                            });
                            vm.loadPage(false);
                        } else {
                            Vue.prototype.$message({
                                type: 'error',
                                message: r.msg
                            });
                        }
                    }
                });
            }).catch(() => {
                this.$message({
                    type: 'info',
                    message: '已取消删除'
                });
            });
        },
        getInfo: function (userId) {
            $.get(baseURL + "sys/user/info/" + userId, function (r) {
                vm.user = r.user;
            });
        },
        getRoleList: function () {
            $.get(baseURL + "sys/role/select", function (r) {
                vm.roleList = r.list;
            });
        },
        loadPage(home) {
            let q = {}
            if (home) {
                q.page = '1'
                this.list(q);
            } else {
                vm.showList=true;
                this.list(vm.q)
            }

        },
        list(data) {
            $.ajax({
                type: "POST",
                url: baseURL + 'sys/user/list',
                contentType: "application/json",
                data: JSON.stringify(data),
                success: function (r) {
                    if (r.code === 0) {
                        vm.page = r.page
                    } else {
                        alert(r.msg);
                    }
                }
            });
        },
        listCompany(){
            $.ajax({
                type: "POST",
                url: baseURL + 'dev/company/companyList',
                contentType: "application/json",
                data: null,
                success: function (r) {
                    if (r.code === 0) {
                        vm.companyList = r.list
                    } else {
                        alert(r.msg);
                    }
                }
            });
        },
        setCompany(){
            $.ajax({
                type: "POST",
                url: baseURL + 'dev/company/setCompany',
                contentType: "application/json",
                data: JSON.stringify(vm.company),
                success: function (r) {
                    if (r.code === 0) {
                        Vue.prototype.$message({
                            type: 'success',
                            message: '设置成功!'
                        });
                        vm.loadPage(false);
                        vm.showCompanyList = false;
                    } else {
                        Vue.prototype.$message({
                            type: 'error',
                            message: r.msg
                        });
                    }
                }
            });
        },
        chooseCompany(event){
            vm.company.companyId=vm.companyId;
        },
        getSelectedIds: function () {
            var ids=[];
            for (var item of vm.multipleSelection) {
                ids.push(item.userId)
            }
            return ids;
        },
    },
    created: function () {
        this.loadPage(true);
        this.listCompany();
    }
});
