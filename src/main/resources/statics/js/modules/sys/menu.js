var baseURL = "../../";
var vm = new Vue({
    el: '#cwapp',
    data: {
        showList: true,
        title: null,
        menu: {
            parentName: null,
            parentId: 0,
            type: 1,
            orderNum: 0,
        },
        menuList: [],
        /*选择上级菜单时用的树形结构*/
        selectMenuList: [
            {
                menuId:0,
                name:"一级菜单",
                children:[]
            }
        ],
        menuType: [
            {
                id: 0,
                name: "目录"
            },
            {
                id: 1,
                name: "菜单"
            },
            {
                id: 2,
                name: "按钮"
            }
        ],
        currentRow: null,/*当前选中的菜单*/
        currentParentMenu:null,/*选中的上级菜单*/
        defaultProps: {
            children: 'children',
            label: 'name'
        },
        visible:false
    },
    methods: {
        closePopover(flag){
            if (flag && vm.currentParentMenu!=null){
                vm.menu.parentId = vm.currentParentMenu.menuId;
                vm.menu.parentName = vm.currentParentMenu.name;
            }
            this.$refs.morePop.doClose()
        },
        getMenuList: function (flag) {
            if (!flag) {
                vm.showList = true;
            }
            $.get(baseURL + "sys/menu/getTreeMenu", function (r) {
                vm.menuList = r.menuList;
            });
        },
        handleCurrentChange(val) {
            this.currentRow = val;
            console.log(this.currentRow)
        },
        handleNodeClick(data) {
            this.currentParentMenu=data;
            console.log(data);
        },
        add: function () {
            vm.showList = false;
            vm.title = "新增系统菜单"
            vm.menu = {parentName: null, parentId: 0, type: 1, orderNum: 0};
            vm.selectMenuList[0].children=vm.menuList;
        },
        update: function (event) {
            if (vm.currentRow == null) {
                return;
            }
            $.get(baseURL + "sys/menu/info/" + vm.currentRow.menuId.toString(), function (r) {
                vm.showList = false;
                vm.title = "修改系统菜单"
                vm.menu = r.menu;
                vm.selectMenuList[0].children=vm.menuList;
            });
        },
        saveOrUpdate: function (event) {
            var url;
            url = vm.menu.menuId == null ? "sys/menu/save" : "sys/menu/update";
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.menu),
                success: function (r) {
                    if (r.code === 0) {
                        Vue.prototype.$message({
                            type: 'success',
                            message: '操作成功!'
                        });
                        vm.getMenuList(false);
                    } else {
                        Vue.prototype.$message({
                            type: 'error',
                            message: r.msg
                        });
                    }
                }
            });
        },
        del: function (event) {
            if (vm.currentRow == null) {
                return;
            }
            this.$confirm('确定要删除选中的记录?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                $.ajax({
                    type: "POST",
                    url: baseURL + "sys/menu/delete",
                    contentType: "application/json",
                    data: JSON.stringify(vm.currentRow.menuId.toString()),
                    success: function (r) {
                        if (r.code === 0) {
                            Vue.prototype.$message({
                                type: 'success',
                                message: '删除成功!'
                            });
                            vm.getMenuList(false);
                        } else {
                            Vue.prototype.$message({
                                type: 'error',
                                message: r.msg
                            });
                        }
                    }
                });
            }).catch(() => {
                this.$message({
                    type: 'info',
                    message: '已取消删除'
                });
            });
        },
        getInfo: function (menuId) {
            $.get(baseURL + "sys/menu/info/" + menuId, function (r) {
                vm.menu = r.menu;
            });
        },
    },
    created: function () {
        this.getMenuList(true)
    }
});
