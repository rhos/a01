
var baseURL = "../../";
var vm = new Vue({
    el: '#cwapp',
    data: {
        showList: true,
        title: null,
        role: {
            deptId: null,
            deptName: null
        },
        q: {
            roleName: null,
            page: "1",
            limit: "10"
        },
        page: {},
        multipleSelection: [],
        /*选择权限时用的树形结构*/
        selectMenuList: [
        ],
        /*默认选中的菜单*/
        menuIdList: [],
        defaultProps: {
            children: 'children',
            label: 'name'
        },
    },
    methods: {
        handleSizeChange(val) {
            console.log(val);
            vm.q.limit = val.toString();
            this.loadPage(false);
        },
        handleCurrentChange(val) {
            console.log(val);
            vm.q.page = val.toString();
            this.loadPage(false);
        },
        handleSelectionChange(val) {
            this.multipleSelection = val;
            console.log(this.multipleSelection)
        },
        loadPage(home) {
            let q = {}
            if (home) {
                q.page = '1'
                this.list(q);
            } else {
                vm.showList = true;
                this.list(vm.q)
            }

        },
        list(data) {
            $.ajax({
                type: "POST",
                url: baseURL + 'sys/role/list',
                contentType: "application/json",
                data: JSON.stringify(data),
                success: function (r) {
                    if (r.code === 0) {
                        vm.page = r.page
                    } else {
                        alert(r.msg);
                    }
                }
            });
        },
        getSelectedIds: function () {
            var ids = [];
            for (var item of vm.multipleSelection) {
                ids.push(item.roleId)
            }
            return ids;
        },
        getMenuList: function () {
            $.get(baseURL + "sys/menu/getTreeMenu", function (r) {
                vm.selectMenuList = r.menuList;
            });
        },
        query: function () {
            this.loadPage(false)
        },
        add: function () {
            vm.showList = false;
            vm.title = "新增";
            vm.role = {deptName: null, deptId: null};
            vm.getMenuList();
        },
        update: function (event) {
            var ids = this.getSelectedIds();
            let roleId;
            if (ids.length === 1) {
                roleId = ids[0];
            } else {
                this.$message.error('请选择一条记录');
                return;
            }
            vm.showList = false;
            vm.title = "修改";
            vm.getMenuList();
            vm.getInfo(roleId)

        },
        getInfo: function (roleId) {
            $.get(baseURL + "sys/role/info/" + roleId.toString(), function (r) {
                vm.role = r.role;
                vm.menuIdList = r.role.menuIdList;
                console.log(vm.menuIdList)
            });
        },
        getCheckedKeys() {
            console.log(this.$refs.tree.getCheckedKeys());
        },
        saveOrUpdate: function (event) {
            vm.role.menuIdList = this.$refs.tree.getCheckedKeys();

            var url = vm.role.roleId == null ? "sys/role/save" : "sys/role/update";
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.role),
                success: function (r) {
                    if (r.code === 0) {
                        Vue.prototype.$message({
                            type: 'success',
                            message: '操作成功!'
                        });
                        vm.loadPage(false);
                    } else {
                        Vue.prototype.$message({
                            type: 'error',
                            message: r.msg
                        });
                    }
                }
            });
        },
        del: function (event) {
            var ids = this.getSelectedIds();
            let roleIds;
            if (ids.length === 0) {
                this.$message.error('请选择一条记录');
                return;
            } else {
                roleIds = ids;
            }
            this.$confirm('确定要删除选中的记录?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                $.ajax({
                    type: "POST",
                    url: baseURL + "sys/role/delete",
                    contentType: "application/json",
                    data: JSON.stringify(roleIds),
                    success: function (r) {
                        if (r.code === 0) {
                            Vue.prototype.$message({
                                type: 'success',
                                message: '删除成功!'
                            });
                            vm.loadPage(false);
                        } else {
                            Vue.prototype.$message({
                                type: 'error',
                                message: r.msg
                            });
                        }
                    }
                });
            }).catch(() => {
                this.$message({
                    type: 'info',
                    message: '已取消删除'
                });
            });
        },
    },
    created: function () {
        this.loadPage(true);
    }
});

