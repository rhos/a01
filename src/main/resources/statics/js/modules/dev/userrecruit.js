var baseURL = "../../";
var vm = new Vue({
	el:'#rrapp',
	data:{
		q: {
			name:null,
			page: "1",
			limit: "10"
		},
        sexList:[
            {
                id:0,
                sex:'男'
            },
            {
                id:1,
                sex:'女'
            }
        ],
        userInfo:{},
        workAttendanceList:[],
        evaluateList:[],
        rewardList:[],
        showUserInfo:false,
        showWorkAttendance:false,
        showEvaluate:false,
        showReward:false,
	    isDisable:false,
		showList: true,
		title: null,
		page: {},
		multipleSelection: [],
		userRecruit: {}
	},
	methods: {
		handleSizeChange(val) {
			console.log(val);
			vm.q.limit = val.toString();
			this.loadPage(false);
		},
		handleCurrentChange(val) {
			console.log(val);
			vm.q.page = val.toString();
			this.loadPage(false);
		},
		handleSelectionChange(val) {
			this.multipleSelection = val;
			console.log(this.multipleSelection)
		},
		query: function () {
			this.loadPage(false)
		},
        selectUserInfo(id){
            $.get(baseURL + "sys/user/info/"+id, function(r){
                vm.userInfo = r.user;
                vm.showUserInfo =true;
            });
        },
        listWorkAttendance(idNumber){
            $.get(baseURL + "dev/workattendance/employeeAttendanceList/"+idNumber, function(r){
                vm.workAttendanceList = r.list;
                vm.showWorkAttendance =true;
            });
        },
        listReward(idNumber){
            $.get(baseURL + "dev/reward/employeeRewardList/"+idNumber, function(r){
                vm.rewardList = r.list;
                vm.showReward =true;
            });
        },
        listEvaluate(idNumber){
            $.get(baseURL + "dev/evaluate/employeeEvaluateList/"+idNumber, function(r){
                vm.evaluateList = r.list;
                vm.showEvaluate =true;
            });
        },
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.userRecruit = {};
		},
		del: function (event) {
            var ids=this.getSelectedIds();
            let selectIds;
            if (ids.length===0){
                this.$message.error('请选择一条记录');
                return;
            }else {
                selectIds=ids;
            }

            this.$confirm('确定要删除选中的记录?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                $.ajax({
                    type: "POST",
                    url: baseURL + "dev/userrecruit/delete",
                    contentType: "application/json",
                    data: JSON.stringify(selectIds),
                    success: function (r) {
                        if (r.code === 0) {
                            Vue.prototype.$message({
                                type: 'success',
                                message: '删除成功!'
                            });
                            vm.loadPage(false);
                        } else {
                            Vue.prototype.$message({
                                type: 'error',
                                message: r.msg
                            });
                        }
                    }
                });
            }).catch(() => {
                this.$message({
                    type: 'info',
                    message: '已取消删除'
                });
            });

		},
		getInfo: function(id){
			$.get(baseURL + "dev/userrecruit/info/"+id, function(r){
                vm.userRecruit = r.userRecruit;
            });
		},
        loadPage(home) {
            let q = {}
            if (home) {
                q.page = '1'
                this.list(q);
            } else {
                vm.showList=true;
                this.list(vm.q)
            }

        },
        list(data) {
            $.ajax({
                type: "POST",
                url: baseURL + 'dev/userrecruit/list',
                contentType: "application/json",
                data: JSON.stringify(data),
                success: function (r) {
                    if (r.code === 0) {
                        vm.page = r.page
                    } else {
                        alert(r.msg);
                    }
                }
            });
        },
        getSelectedIds: function () {
            var ids=[];
            for (var item of vm.multipleSelection) {
                ids.push(item.id)
            }
            return ids;
        }
	},
    created: function () {
        this.loadPage(true);
    }
});
