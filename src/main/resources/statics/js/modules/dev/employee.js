var baseURL = "../../";
var vm = new Vue({
	el:'#rrapp',
	data:{
		q: {
			name:null,
			page: "1",
			limit: "10"
		},
        sexList:[
            {
                id:0,
                sex:'男'
            },
            {
                id:1,
                sex:'女'
            }
        ],
        statusList:[
            {
                id:2,
                name:'在职'
            },
            {
                id:1,
                name:'离职'
            }
        ],
        user:{},
	    isDisable:false,
		showList: true,
		title: null,
		page: {},
		multipleSelection: [],
		employee: {},
        companyName:""
	},
	methods: {
		handleSizeChange(val) {
			console.log(val);
			vm.q.limit = val.toString();
			this.loadPage(false);
		},
		handleCurrentChange(val) {
			console.log(val);
			vm.q.page = val.toString();
			this.loadPage(false);
		},
		handleSelectionChange(val) {
			this.multipleSelection = val;
			console.log(this.multipleSelection)
		},
		query: function () {
			this.loadPage(false)
		},
        handleItem(id){

        },
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.employee = {};
		},
		update: function (event) {
			var ids=this.getSelectedIds();
			let id;
			if (ids.length===1){
				id=ids[0];
			}else {
				this.$message.error('请选择一条记录');
				return;
			}

			vm.showList = false;
            vm.title = "修改";

            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
            var url = vm.employee.id == null ? "dev/employee/save" : "dev/employee/update";
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.employee),
                // data: vm.user,
                success: function (r) {
                    if (r.code === 0) {
                        Vue.prototype.$message({
                            type: 'success',
                            message: '操作成功!'
                        });
                        vm.loadPage(false);
                    } else {
                        Vue.prototype.$message({
                            type: 'error',
                            message: r.msg
                        });
                    }
                }
            });
		},
		del: function (event) {
            var ids=this.getSelectedIds();
            let selectIds;
            if (ids.length===0){
                this.$message.error('请选择一条记录');
                return;
            }else {
                selectIds=ids;
            }

            this.$confirm('确定要删除选中的记录?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                $.ajax({
                    type: "POST",
                    url: baseURL + "dev/employee/delete",
                    contentType: "application/json",
                    data: JSON.stringify(selectIds),
                    success: function (r) {
                        if (r.code === 0) {
                            Vue.prototype.$message({
                                type: 'success',
                                message: '删除成功!'
                            });
                            vm.loadPage(false);
                        } else {
                            Vue.prototype.$message({
                                type: 'error',
                                message: r.msg
                            });
                        }
                    }
                });
            }).catch(() => {
                this.$message({
                    type: 'info',
                    message: '已取消删除'
                });
            });

		},
		getInfo: function(id){
			$.get(baseURL + "dev/employee/info/"+id, function(r){
                vm.employee = r.employee;
            });
		},
        loadPage(home) {
            let q = {}
            if (home) {
                q.page = '1'
                this.list(q);
            } else {
                vm.showList=true;
                this.list(vm.q)
            }

        },
        list(data) {
            $.ajax({
                type: "POST",
                url: baseURL + 'dev/employee/list',
                contentType: "application/json",
                data: JSON.stringify(data),
                success: function (r) {
                    if (r.code === 0) {
                        vm.page = r.page
                    } else {
                        alert(r.msg);
                    }
                }
            });
        },
        getUser: function () {
            $.getJSON(baseURL +"getLoginUserInfo", function (r) {
                vm.companyName=r.company.name
            });
        },
        getSelectedIds: function () {
            var ids=[];
            for (var item of vm.multipleSelection) {
                ids.push(item.id)
            }
            return ids;
        }
	},
    created: function () {
        this.loadPage(true);
        this.getUser();
    }
});
