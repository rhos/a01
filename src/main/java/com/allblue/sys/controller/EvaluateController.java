package com.allblue.sys.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.allblue.sys.entity.EmployeeEntity;
import com.allblue.sys.entity.EvaluateEntity;
import com.allblue.sys.entity.RewardEntity;
import com.allblue.sys.entity.UserEntity;
import com.allblue.sys.service.EmployeeService;
import com.allblue.sys.service.EvaluateService;
import com.allblue.utils.ContextHolderUtils;
import com.allblue.utils.RRException;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.allblue.utils.PageUtils;
import com.allblue.utils.R;



/**
 * 评价
 * @date 2022-03-01 13:43:21
 */
@RestController
@RequestMapping("dev/evaluate")
public class EvaluateController {
    @Autowired
    private EvaluateService evaluateService;
    @Autowired
    private EmployeeService employeeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestBody Map<String, Object> params){
        PageUtils page = evaluateService.queryPage(params);

        return R.ok().put("page", page);
    }
    /**
     * 身份证号查询奖惩列表
     */
    @RequestMapping("/employeeEvaluateList/{idNumber}")
    public R employeeEvaluateList(@PathVariable("idNumber") String idNumber){
        List<EvaluateEntity> list = evaluateService.selectList(new EntityWrapper<EvaluateEntity>().eq("id_number", idNumber));
        return R.ok().put("list", list);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        EvaluateEntity evaluate = evaluateService.selectById(id);

        return R.ok().put("evaluate", evaluate);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody EvaluateEntity evaluate){
        EmployeeEntity employeeEntity = employeeService.selectById(evaluate.getEmployeeId());
        if (employeeEntity == null) {
            throw new RRException("员工不存在");
        }
        UserEntity user = ContextHolderUtils.getUser();
        evaluate.setCompanyId(user.getCompanyId());
        evaluate.setIdNumber(employeeEntity.getIdNumber());
        evaluate.setCreateDate(new Date());
        evaluateService.insert(evaluate);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody EvaluateEntity evaluate){
        EmployeeEntity employeeEntity = employeeService.selectById(evaluate.getEmployeeId());
        if (employeeEntity == null) {
            throw new RRException("员工不存在");
        }
        UserEntity user = ContextHolderUtils.getUser();
        evaluate.setCompanyId(user.getCompanyId());
        evaluate.setIdNumber(employeeEntity.getIdNumber());
        evaluate.setCreateDate(new Date());
        evaluateService.updateAllColumnById(evaluate);//全部更新

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        evaluateService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
