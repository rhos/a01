package com.allblue.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.allblue.sys.entity.EmployeeEntity;
import com.allblue.sys.entity.UserEntity;
import com.allblue.sys.service.EmployeeService;
import com.allblue.utils.ContextHolderUtils;
import com.allblue.utils.RRException;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.allblue.utils.PageUtils;
import com.allblue.utils.R;



/**
 * 
 * @date 2022-03-01 13:43:21
 */
@RestController
@RequestMapping("dev/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestBody Map<String, Object> params){
        PageUtils page = employeeService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 查询公司所有员工
     */
    @RequestMapping("/listCompanyEmployee")
    public R listCompanyEmployee(){
        List<EmployeeEntity> employeeEntityList= employeeService.listEmployee();
        return R.ok().put("list", employeeEntityList);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        EmployeeEntity employee = employeeService.selectById(id);

        return R.ok().put("employee", employee);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody EmployeeEntity employee){
        int count = employeeService.selectCount(new EntityWrapper<EmployeeEntity>().eq("id_number", employee.getIdNumber()));
        if (count > 0) {
            throw new RRException("身份证号已存在");
        }
        UserEntity user = ContextHolderUtils.getUser();
        employee.setCompanyId(user.getCompanyId());
        employeeService.insert(employee);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody EmployeeEntity employee){
        int count = employeeService.selectCount(new EntityWrapper<EmployeeEntity>().eq("id_number", employee.getIdNumber()).ne("id",employee.getId()));
        if (count > 0) {
            throw new RRException("身份证号已存在");
        }
        UserEntity user = ContextHolderUtils.getUser();
        employee.setCompanyId(user.getCompanyId());
        employeeService.updateAllColumnById(employee);//全部更新
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        employeeService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
