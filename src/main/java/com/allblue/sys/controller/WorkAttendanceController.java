package com.allblue.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.allblue.sys.entity.EmployeeEntity;
import com.allblue.sys.entity.UserEntity;
import com.allblue.sys.entity.WorkAttendanceEntity;
import com.allblue.sys.service.EmployeeService;
import com.allblue.sys.service.WorkAttendanceService;
import com.allblue.utils.ContextHolderUtils;
import com.allblue.utils.RRException;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.allblue.utils.PageUtils;
import com.allblue.utils.R;



/**
 * 考勤
 * @date 2022-03-01 13:43:21
 */
@RestController
@RequestMapping("dev/workattendance")
public class WorkAttendanceController {
    @Autowired
    private WorkAttendanceService workAttendanceService;
    @Autowired
    private EmployeeService employeeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestBody Map<String, Object> params){
        PageUtils page = workAttendanceService.queryPage(params);

        return R.ok().put("page", page);
    }
    /**
     * 身份证号查询出勤列表
     */
    @RequestMapping("/employeeAttendanceList/{idNumber}")
    public R employeeAttendanceList(@PathVariable("idNumber") String idNumber){
        List<WorkAttendanceEntity> list = workAttendanceService.selectList(new EntityWrapper<WorkAttendanceEntity>().eq("id_number", idNumber));
        return R.ok().put("list", list);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        WorkAttendanceEntity workAttendance = workAttendanceService.selectById(id);

        return R.ok().put("workAttendance", workAttendance);
    }


    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody WorkAttendanceEntity workAttendance){
        EmployeeEntity employeeEntity = employeeService.selectById(workAttendance.getEmployeeId());
        if (employeeEntity == null) {
            throw new RRException("员工不存在");
        }
        UserEntity user = ContextHolderUtils.getUser();
        workAttendance.setCompanyId(user.getCompanyId());
        workAttendance.setIdNumber(employeeEntity.getIdNumber());
        workAttendanceService.insert(workAttendance);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody WorkAttendanceEntity workAttendance){
        EmployeeEntity employeeEntity = employeeService.selectById(workAttendance.getEmployeeId());
        if (employeeEntity == null) {
            throw new RRException("员工不存在");
        }
        UserEntity user = ContextHolderUtils.getUser();
        workAttendance.setCompanyId(user.getCompanyId());
        workAttendance.setIdNumber(employeeEntity.getIdNumber());
        workAttendanceService.updateAllColumnById(workAttendance);//全部更新

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        workAttendanceService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
