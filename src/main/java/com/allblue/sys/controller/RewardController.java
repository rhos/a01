package com.allblue.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.allblue.sys.entity.EmployeeEntity;
import com.allblue.sys.entity.RewardEntity;
import com.allblue.sys.entity.UserEntity;
import com.allblue.sys.entity.WorkAttendanceEntity;
import com.allblue.sys.service.EmployeeService;
import com.allblue.sys.service.RewardService;
import com.allblue.utils.ContextHolderUtils;
import com.allblue.utils.RRException;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.allblue.utils.PageUtils;
import com.allblue.utils.R;



/**
 * 奖惩峰会突然 分割
 * @date 2022-03-01 13:43:21
 */
@RestController
@RequestMapping("dev/reward")
public class RewardController {
    @Autowired
    private RewardService rewardService;
    @Autowired
    private EmployeeService employeeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestBody Map<String, Object> params){
        PageUtils page = rewardService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 身份证号查询奖惩列表
     */
    @RequestMapping("/employeeRewardList/{idNumber}")
    public R employeeRewardList(@PathVariable("idNumber") String idNumber){
        List<RewardEntity> list = rewardService.selectList(new EntityWrapper<RewardEntity>().eq("id_number", idNumber));
        return R.ok().put("list", list);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        RewardEntity reward = rewardService.selectById(id);

        return R.ok().put("reward", reward);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody RewardEntity reward){
        EmployeeEntity employeeEntity = employeeService.selectById(reward.getEmployeeId());
        if (employeeEntity == null) {
            throw new RRException("员工不存在");
        }
        UserEntity user = ContextHolderUtils.getUser();
        reward.setCompanyId(user.getCompanyId());
        reward.setIdNumber(employeeEntity.getIdNumber());
        rewardService.insert(reward);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody RewardEntity reward){
        rewardService.updateAllColumnById(reward);//全部更新

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        rewardService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
