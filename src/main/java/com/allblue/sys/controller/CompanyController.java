package com.allblue.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.allblue.sys.entity.CompanyEntity;
import com.allblue.sys.entity.UserEntity;
import com.allblue.sys.service.CompanyService;
import com.allblue.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.allblue.utils.PageUtils;
import com.allblue.utils.R;



/**
 * 
 * @date 2022-03-01 13:43:21
 */
@RestController
@RequestMapping("dev/company")
public class CompanyController {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private UserService userService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestBody Map<String, Object> params){
        PageUtils page = companyService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 公司列表
     */
    @RequestMapping("/companyList")
    public R companyList(){
        List<CompanyEntity> companyEntities = companyService.selectList(null);
        return R.ok().put("list", companyEntities);
    }
    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        CompanyEntity company = companyService.selectById(id);

        return R.ok().put("company", company);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody CompanyEntity company){
        companyService.insert(company);
        return R.ok();
    }

    /**
     * 设置公司
     */
    @RequestMapping("/setCompany")
    public R setCompany(@RequestBody CompanyEntity company){
        UserEntity userEntity = userService.selectById(company.getUserId());
        userEntity.setCompanyId(company.getCompanyId());
        userService.updateById(userEntity);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CompanyEntity company){
        companyService.updateAllColumnById(company);//全部更新

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        companyService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
