package com.allblue.sys.controller;

import java.util.Arrays;
import java.util.Map;

import com.allblue.sys.entity.RecruitEntity;
import com.allblue.sys.entity.UserEntity;
import com.allblue.sys.entity.UserRecruitEntity;
import com.allblue.sys.service.RecruitService;
import com.allblue.sys.service.UserRecruitService;
import com.allblue.sys.service.UserService;
import com.allblue.utils.ContextHolderUtils;
import com.allblue.utils.RRException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.allblue.utils.PageUtils;
import com.allblue.utils.R;



/**
 * 
 * @date 2022-03-01 13:43:21
 */
@RestController
@RequestMapping("dev/userrecruit")
public class UserRecruitController {
    @Autowired
    private UserRecruitService userRecruitService;
    @Autowired
    private RecruitService recruitService;
    @Autowired
    private UserService userService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestBody Map<String, Object> params){
        PageUtils page = userRecruitService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        UserRecruitEntity userRecruit = userRecruitService.selectById(id);

        return R.ok().put("userRecruit", userRecruit);
    }

    /**
     * 保存
     */
    @RequestMapping("/send/{id}")
    public R save(@PathVariable("id")Long id){
        UserEntity user = ContextHolderUtils.getUser();
        UserEntity userDB = userService.selectById(user.getUserId());
        if (StringUtils.isBlank(userDB.getChineseName()) || StringUtils.isBlank(userDB.getIdNumber())) {
            throw new RRException("请先完善个人信息");
        }
        UserRecruitEntity userRecruitEntity = new UserRecruitEntity();
        userRecruitEntity.setUserId(user.getUserId());
        userRecruitEntity.setIdNumber(user.getIdNumber());
        userRecruitEntity.setRecruitId(id);
        RecruitEntity recruitEntity = recruitService.selectById(id);
        userRecruitEntity.setCompanyId(recruitEntity.getCompanyId());
        userRecruitService.insert(userRecruitEntity);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody UserRecruitEntity userRecruit){
        userRecruitService.updateAllColumnById(userRecruit);//全部更新

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        userRecruitService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
