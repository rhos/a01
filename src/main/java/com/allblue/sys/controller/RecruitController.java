package com.allblue.sys.controller;

import java.util.Arrays;
import java.util.Map;

import com.allblue.sys.entity.RecruitEntity;
import com.allblue.sys.entity.UserEntity;
import com.allblue.sys.service.RecruitService;
import com.allblue.utils.ContextHolderUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.allblue.utils.PageUtils;
import com.allblue.utils.R;



/**
 * 招聘信息
 * @date 2022-03-01 13:43:21
 */
@RestController
@RequestMapping("dev/recruit")
public class RecruitController {
    @Autowired
    private RecruitService recruitService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestBody Map<String, Object> params){
        PageUtils page = recruitService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 公司发布的
     */
    @RequestMapping("/myCreatePage")
    public R myCreatePage(@RequestBody Map<String, Object> params){
        PageUtils page = recruitService.myCreatePage(params);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        RecruitEntity recruit = recruitService.selectById(id);

        return R.ok().put("recruit", recruit);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody RecruitEntity recruit){
        UserEntity user = ContextHolderUtils.getUser();
        recruit.setCompanyId(user.getCompanyId());
        recruit.setUserId(user.getUserId());
        recruitService.insert(recruit);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody RecruitEntity recruit){
        UserEntity user = ContextHolderUtils.getUser();
        recruit.setCompanyId(user.getCompanyId());
        recruitService.updateAllColumnById(recruit);//全部更新

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        recruitService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
