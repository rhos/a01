package com.allblue.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 评价
 * @date 2022-03-01 13:43:21
 */
@TableName(value = "evaluate",resultMap = "evaluateMap")
@Data
public class EvaluateEntity implements Serializable {

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 公司id
	 */
	private Long companyId;
	@TableField(exist = false)
	private String companyName;
	/**
	 * 员工id
	 */
	private Long employeeId;
	@TableField(exist = false)
	private String employeeName;
	/**
	 * 身份证号
	 */
	private String idNumber;
	/**
	 * 评价
	 */
	private String describe;
	/**
	 * 日期
	 */
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
	private Date createDate;

}
