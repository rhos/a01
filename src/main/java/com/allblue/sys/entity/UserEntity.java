package com.allblue.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @date 2020-06-16 09:04:34
 */
@TableName(value = "sys_user",resultMap = "tUserMap")
@Data
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long userId;
	/**
	 * 账户
	 */
	private String username;
	/**
	 * 密码
	 */
	private String password;
	private Long companyId;
	@TableField(exist = false)
	private CompanyEntity company;
	/**
	 * 用户姓名
	 */
	private String chineseName;
	/**
	 * 0.男 1.女
	 */
	private int sex ;
	private String idNumber;
	private int age;
	private String education;
	private String major;

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	@TableField(exist=false)
	private Long roleId;
	@TableField(exist=false)
	private String roleName;
}
