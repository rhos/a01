package com.allblue.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 
 * @date 2022-03-01 13:43:21
 */
@TableName(value = "employee",resultMap = "employeeMap")
@Data
public class EmployeeEntity implements Serializable {

	/**
	 * 
	 */
	@TableId
	private Long id;
	private String name;
	/**
	 * 公司id
	 */
	private Long companyId;
	@TableField(exist = false)
	private String companyName;
	/**
	 * 身份证号
	 */
	private String idNumber;
	/**
	 * 0.男 1.女
	 */
	private Integer sex;
	/**
	 * 生日
	 */
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
	private Date birthday;
	/**
	 * 电话号码
	 */
	private String phoneNumber;
	/**
	 * 部门
	 */
	private String dept;
	/**
	 * 职位
	 */
	private String post;
	/**
	 * 1. 离职  2.在职
	 */
	private Integer status;

}
