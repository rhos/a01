package com.allblue.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @date 2022-03-01 13:43:21
 */
@TableName(value = "user_recruit",resultMap = "userRecruitMap")
@Data
public class UserRecruitEntity implements Serializable {

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 用户id
	 */
	private Long userId;
	@TableField(exist = false)
	private String chineseName;
	/**
	 * 身份证号
	 */
	private String idNumber;
	/**
	 * 招聘信息id
	 */
	private Long recruitId;
	@TableField(exist = false)
	private RecruitEntity recruit;
	/**
	 * 公司id
	 */
	private Long companyId;
}
