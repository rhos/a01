package com.allblue.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @date 2022-03-01 13:43:21
 */
@TableName(value = "company",resultMap = "companyMap")
@Data
public class CompanyEntity implements Serializable {

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 规模
	 */
	private Integer num;
	/**
	 * 简介
	 */
	private String describe;

	@TableField(exist = false)
	private Long userId;
	@TableField(exist = false)
	private Long companyId;

	public Long getCompanyId() {
		return companyId;
	}
}
