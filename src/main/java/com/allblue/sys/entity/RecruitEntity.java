package com.allblue.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 招聘信息
 * @date 2022-03-01 13:43:21
 */
@TableName(value = "recruit",resultMap = "recruitMap")
@Data
public class RecruitEntity implements Serializable {

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 公司id
	 */
	private Long companyId;
	@TableField(exist = false)
	private String companyName;
	/**
	 * 描述
	 */
	private String describes;
	/**
	 * 发布日期
	 */
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
	private Date createDate;
	/**
	 * 用户id
	 */
	private Long userId;
	@TableField(exist = false)
	private String chineseName;
	/**
	 * 是否已投递
	 */
	@TableField(exist = false)
	private boolean send = false;

}
