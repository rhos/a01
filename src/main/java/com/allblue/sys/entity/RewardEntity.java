package com.allblue.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 奖惩
 * @date 2022-03-01 13:43:21
 */
@TableName(value = "reward",resultMap = "rewardMap")
@Data
public class RewardEntity implements Serializable {

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 公司id
	 */
	private Long companyId;
	@TableField(exist = false)
	private String companyName;
	/**
	 * 员工id
	 */
	private Long employeeId;
	@TableField(exist = false)
	private String employeeName;
	/**
	 * 身份证号
	 */
	private String idNumber;
	/**
	 * 描述
	 */
	private String describe;
	/**
	 * 0奖励 1惩罚
	 */
	private Integer type;
	/**
	 * 创建日期
	 */
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
	private Date createDate;

}
