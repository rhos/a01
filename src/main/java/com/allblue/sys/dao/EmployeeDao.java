package com.allblue.sys.dao;

import com.allblue.sys.entity.EmployeeEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
 * @date 2022-03-01 13:43:21
 */
public interface EmployeeDao extends BaseMapper<EmployeeEntity> {
	
}
