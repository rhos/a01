package com.allblue.sys.dao;

import com.allblue.sys.entity.RecruitEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 招聘信息
 * @date 2022-03-01 13:43:21
 */
public interface RecruitDao extends BaseMapper<RecruitEntity> {
	
}
