package com.allblue.sys.dao;

import com.allblue.sys.entity.RewardEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 奖惩
 * @date 2022-03-01 13:43:21
 */
public interface RewardDao extends BaseMapper<RewardEntity> {
	
}
