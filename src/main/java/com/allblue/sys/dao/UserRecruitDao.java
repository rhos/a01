package com.allblue.sys.dao;

import com.allblue.sys.entity.UserRecruitEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 
 * @date 2022-03-01 13:43:21
 */
public interface UserRecruitDao extends BaseMapper<UserRecruitEntity> {
	
}
