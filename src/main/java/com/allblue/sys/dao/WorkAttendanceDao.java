package com.allblue.sys.dao;


import com.allblue.sys.entity.WorkAttendanceEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 考勤
 * @date 2022-03-01 13:43:21
 */
public interface WorkAttendanceDao extends BaseMapper<WorkAttendanceEntity> {
	
}
