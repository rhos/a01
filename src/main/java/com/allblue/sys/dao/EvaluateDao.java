package com.allblue.sys.dao;

import com.allblue.sys.entity.EvaluateEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 评价
 * @date 2022-03-01 13:43:21
 */
public interface EvaluateDao extends BaseMapper<EvaluateEntity> {
	
}
