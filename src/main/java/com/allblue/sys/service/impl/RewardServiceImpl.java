package com.allblue.sys.service.impl;

import com.allblue.sys.entity.EmployeeEntity;
import com.allblue.sys.entity.UserEntity;
import com.allblue.sys.service.EmployeeService;
import com.allblue.utils.ContextHolderUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.allblue.utils.PageUtils;
import com.allblue.utils.Query;

import com.allblue.sys.dao.RewardDao;
import com.allblue.sys.entity.RewardEntity;
import com.allblue.sys.service.RewardService;


@Service("rewardService")
public class RewardServiceImpl extends ServiceImpl<RewardDao, RewardEntity> implements RewardService {
    @Autowired
    private EmployeeService employeeService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String) params.get("name");
        String idNumber = (String) params.get("idNumber");
        UserEntity user = ContextHolderUtils.getUser();
        List<EmployeeEntity> employeeEntityList = employeeService.selectList(new EntityWrapper<EmployeeEntity>()
                .like(StringUtils.isNotBlank(name), "name", name).eq("company_id",user.getCompanyId()));
        if (CollectionUtils.isEmpty(employeeEntityList)) {
            return new PageUtils(new Page<>());
        }
        List<Long> employeeIds = employeeEntityList.stream().map(EmployeeEntity::getId).collect(Collectors.toList());
        EntityWrapper<RewardEntity> ew = new EntityWrapper<>();
        ew.eq(StringUtils.isNotBlank(idNumber),"id_number",idNumber);
        ew.in("employee_id",employeeIds);
        ew.orderBy("create_date",false);
        Page<RewardEntity> page = this.selectPage(
                new Query<RewardEntity>(params).getPage(),
                ew
        );

        return new PageUtils(page);
    }

}
