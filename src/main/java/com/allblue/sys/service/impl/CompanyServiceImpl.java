package com.allblue.sys.service.impl;

import com.allblue.utils.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.allblue.utils.Query;

import com.allblue.sys.dao.CompanyDao;
import com.allblue.sys.entity.CompanyEntity;
import com.allblue.sys.service.CompanyService;


@Service("companyService")
public class CompanyServiceImpl extends ServiceImpl<CompanyDao, CompanyEntity> implements CompanyService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String)params.get("name");
        EntityWrapper<CompanyEntity> ew = new EntityWrapper<>();
        ew.like(StringUtils.isNotBlank(name),"name",name);
        Page<CompanyEntity> page = this.selectPage(
                new Query<CompanyEntity>(params).getPage(),
                ew
        );
        return new PageUtils(page);
    }

}
