package com.allblue.sys.service.impl;

import com.allblue.sys.entity.UserEntity;
import com.allblue.sys.entity.UserRecruitEntity;
import com.allblue.sys.service.UserRecruitService;
import com.allblue.utils.ContextHolderUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.allblue.utils.PageUtils;
import com.allblue.utils.Query;

import com.allblue.sys.dao.RecruitDao;
import com.allblue.sys.entity.RecruitEntity;
import com.allblue.sys.service.RecruitService;


@Service("recruitService")
public class RecruitServiceImpl extends ServiceImpl<RecruitDao, RecruitEntity> implements RecruitService {
    @Autowired
    private UserRecruitService userRecruitService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String) params.get("name");
        Integer companyId = (Integer) params.get("companyId");
        EntityWrapper<RecruitEntity> ew = new EntityWrapper<>();
        ew.eq(companyId != null, "company_id", companyId);
        ew.like(StringUtils.isNotBlank(name), "describes", name);
        ew.orderBy("create_date", false);
        Page<RecruitEntity> page = this.selectPage(
                new Query<RecruitEntity>(params).getPage(),
                ew
        );
        List<RecruitEntity> records = page.getRecords();
        UserEntity user = ContextHolderUtils.getUser();
        for (RecruitEntity record : records) {
            int count = userRecruitService.selectCount(new EntityWrapper<UserRecruitEntity>().eq("user_id", user.getUserId())
                    .eq("recruit_id", record.getId()));
            if (count > 0) {
                record.setSend(true);
            }
        }
        return new PageUtils(page);
    }

    @Override
    public PageUtils myCreatePage(Map<String, Object> params) {
        String name = (String) params.get("name");
        EntityWrapper<RecruitEntity> ew = new EntityWrapper<>();
        ew.like(StringUtils.isNotBlank(name), "'describe'", name);
        ew.orderBy("create_date", false);
        UserEntity user = ContextHolderUtils.getUser();
        ew.eq("company_id", user.getCompanyId());
        Page<RecruitEntity> page = this.selectPage(
                new Query<RecruitEntity>(params).getPage(),
                ew
        );
        return new PageUtils(page);
    }

}
