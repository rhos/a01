package com.allblue.sys.service.impl;

import com.allblue.sys.entity.UserEntity;
import com.allblue.utils.ContextHolderUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.allblue.utils.PageUtils;
import com.allblue.utils.Query;

import com.allblue.sys.dao.EmployeeDao;
import com.allblue.sys.entity.EmployeeEntity;
import com.allblue.sys.service.EmployeeService;


@Service("employeeService")
public class EmployeeServiceImpl extends ServiceImpl<EmployeeDao, EmployeeEntity> implements EmployeeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String)params.get("name");
        EntityWrapper<EmployeeEntity> ew = new EntityWrapper<>();
        UserEntity user = ContextHolderUtils.getUser();
        if (user.getUserId() != 1) {
            ew.eq("company_id",user.getCompanyId());
        }
        ew.orderBy("status",false);
        ew.like(StringUtils.isNotBlank(name),"name",name);
        Page<EmployeeEntity> page = this.selectPage(
                new Query<EmployeeEntity>(params).getPage(),
                ew
        );

        return new PageUtils(page);
    }

    @Override
    public List<EmployeeEntity> listEmployee() {
        EntityWrapper<EmployeeEntity> ew = new EntityWrapper<>();
        UserEntity user = ContextHolderUtils.getUser();
        ew.eq("company_id",user.getCompanyId());
        ew.eq("status",2);
        return this.selectList(ew);
    }

}
