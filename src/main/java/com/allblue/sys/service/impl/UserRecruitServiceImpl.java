package com.allblue.sys.service.impl;

import com.allblue.sys.entity.EmployeeEntity;
import com.allblue.sys.entity.UserEntity;
import com.allblue.sys.service.EmployeeService;
import com.allblue.sys.service.UserService;
import com.allblue.utils.ContextHolderUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.allblue.utils.PageUtils;
import com.allblue.utils.Query;

import com.allblue.sys.dao.UserRecruitDao;
import com.allblue.sys.entity.UserRecruitEntity;
import com.allblue.sys.service.UserRecruitService;


@Service("userRecruitService")
public class UserRecruitServiceImpl extends ServiceImpl<UserRecruitDao, UserRecruitEntity> implements UserRecruitService {
    @Autowired
    private UserService userService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String) params.get("name");
        String idNumber = (String) params.get("idNumber");
        UserEntity user = ContextHolderUtils.getUser();
        List<UserEntity> userEntityList = userService.selectList(new EntityWrapper<UserEntity>()
                .like(StringUtils.isNotBlank(name), "chinese_name", name));
        if (CollectionUtils.isEmpty(userEntityList)) {
            return new PageUtils(new Page<>());
        }
        List<Long> userIds = userEntityList.stream().map(UserEntity::getUserId).collect(Collectors.toList());
        EntityWrapper<UserRecruitEntity> ew = new EntityWrapper<>();
        ew.eq("company_id",user.getCompanyId());
        ew.in("user_id",userIds);
        Page<UserRecruitEntity> page = this.selectPage(
                new Query<UserRecruitEntity>(params).getPage(),
                ew
        );
        return new PageUtils(page);
    }

}
