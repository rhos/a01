package com.allblue.sys.service;

import com.allblue.sys.entity.RewardEntity;
import com.allblue.utils.PageUtils;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * 奖惩
 * @date 2022-03-01 13:43:21
 */
public interface RewardService extends IService<RewardEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

