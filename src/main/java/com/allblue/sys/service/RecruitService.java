package com.allblue.sys.service;

import com.allblue.sys.entity.RecruitEntity;
import com.allblue.utils.PageUtils;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * 招聘信息
 * @date 2022-03-01 13:43:21
 */
public interface RecruitService extends IService<RecruitEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils myCreatePage(Map<String, Object> params);

}

