package com.allblue.sys.service;

import com.allblue.sys.entity.WorkAttendanceEntity;
import com.allblue.utils.PageUtils;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * 考勤
 * @date 2022-03-01 13:43:21
 */
public interface WorkAttendanceService extends IService<WorkAttendanceEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

