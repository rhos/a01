package com.allblue.sys.service;

import com.allblue.sys.entity.CompanyEntity;
import com.allblue.utils.PageUtils;
import com.baomidou.mybatisplus.service.IService;


import java.util.Map;

/**
 * 
 * @date 2022-03-01 13:43:21
 */
public interface CompanyService extends IService<CompanyEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

