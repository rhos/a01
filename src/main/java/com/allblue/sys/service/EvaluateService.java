package com.allblue.sys.service;

import com.allblue.sys.entity.EvaluateEntity;
import com.allblue.utils.PageUtils;
import com.baomidou.mybatisplus.service.IService;


import java.util.Map;

/**
 * 评价
 * @date 2022-03-01 13:43:21
 */
public interface EvaluateService extends IService<EvaluateEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

