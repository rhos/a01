package com.allblue.utils;

import org.springframework.web.multipart.MultipartFile;


/**
 * @Date:2021/4/10
 */
public class UploadUtils {
    /**
     * 上传图片
     *
     * @return 图片id
     */
    public static String Upload(MultipartFile file) {
        if (file != null) {
            String fileName = System.currentTimeMillis() + "";
            String rootPath = "D:/image/";
            MultipartFileToFile.ToFile(file, rootPath + fileName);
            return fileName;
        }
        return null;
    }
}
